from unittest import TestCase
from blog import Blog 


class BlogTest(TestCase):
    def test_create_blog(self):
        b = Blog('Test', 'Test Author')

        self.assertEqual('Test', b.title)
        self.assertEqual('Test Author', b.author)
        
        expected = []
        self.assertListEqual(expected, b.posts)

    def test_repr(self):
        b = Blog('Test','Test Author')
        b2 = Blog('My Day','Rolf')
        
        self.assertEqual('Test by Test Author (0 posts)', b.__repr__())
        self.assertEqual('My Day by Rolf (0 posts)', b2.__repr__())


    def test_multiple_repr(self):
        b = Blog('Test','Test Author')
        b.posts = ['test']
       
        b2 = Blog('Test','Test Author')
        b2.posts = ['test', 'another']

        self.assertEqual('Test by Test Author (1 post)', b.__repr__())
        self.assertEqual('Test by Test Author (2 posts)', b2.__repr__())
