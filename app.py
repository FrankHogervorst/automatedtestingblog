from blog import Blog

MENU_PROMPT = 'Enter "c" to create a blog, "l" to list blogs, "r" to read blog, "p" to create a post and "q" to quit'
CREATE_BLOG_PROMPT_TITLE = 'Insert blog title: ' 
CREATE_BLOG_PROMPT_AUTHOR = 'Insert blog author: '


READ_BLOG_PROMPT = 'Which blog do you want to read? Insert title: '
POST_TEMPLATE = '''
--- {} ---

{}
'''
CREATE_POST_PROMPT = 'For which blog do you want to write a post? '
CREATE_POST_PROMPT_TITLE = 'Provide title of post: '
CREATE_POST_PROMPT_CONTENT = 'Provide content of post: '

blogs = dict() # blog name : blog object

def menu():
    # Show user the available blogs
    # let user make a choice
    # Do something with the choice
    # Exit
    selection = input(MENU_PROMPT)

    while selection != 'q': 
        if selection == 'c':
            ask_create_blog()
        elif selection == 'l':
            print_blogs()
        elif selection == 'r': 
            ask_read_blog()
        elif selection == 'p': 
            ask_create_post()
        
        selection = input(MENU_PROMPT)


def print_blogs():
    # print available blogs
    for key, blog in blogs.items():
        print('- {}'.format(blog))

def ask_create_blog():
    title = input(CREATE_BLOG_PROMPT_TITLE)
    author = input(CREATE_BLOG_PROMPT_AUTHOR)

    blog = Blog(title, author)
    blogs[title] = blog

def ask_read_blog():
    title = input(READ_BLOG_PROMPT)

    print_posts(blogs[title])

def print_posts(blog):
    for post in blog.posts: 
        print_post(post)

def print_post(post): 
    print(POST_TEMPLATE.format(post.title, post.content))

def ask_create_post(): 
    blog_title = input(CREATE_POST_PROMPT)
    post_title =  input(CREATE_POST_PROMPT_TITLE)
    post_content = input(CREATE_POST_PROMPT_CONTENT)

    blogs[blog_title].create_post(post_title, post_content)
